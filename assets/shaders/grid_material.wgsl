@group(1) @binding(0)
var grid_texture: texture_2d<f32>;

@group(1) @binding(1)
var grid_sampler: sampler;

@fragment
fn fragment(
    #import bevy_pbr::mesh_vertex_output
) -> @location(0) vec4<f32> {
     return textureSample(grid_texture, grid_sampler, uv);
}
