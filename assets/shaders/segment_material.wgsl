struct CustomMaterial {
    color: vec4<f32>,
};

@group(1) @binding(0)
var<uniform> material: CustomMaterial;

let border: f32 = .1;
let border_radius: f32 = .2;
let inner_radius: f32 = .1;
let gradient_intensity: f32 = 1.5;

fn is_alpha(uv: vec2<f32>) -> f32 {
    if uv.x < border_radius && uv.y < border_radius {
        return ceil(distance(uv, vec2<f32>(border_radius, border_radius)) - border_radius);
    }
    if uv.x > 1. - border_radius && uv.y < border_radius {
        return ceil(distance(uv, vec2<f32>(1. - border_radius, border_radius)) - border_radius);
    }
    if uv.x > 1. - border_radius && uv.y > 1. -  border_radius {
        return ceil(distance(uv, vec2<f32>(1. - border_radius, 1. - border_radius)) - border_radius);
    }
     if uv.x < border_radius && uv.y > 1. - border_radius {
        return ceil(distance(uv, vec2<f32>(border_radius, 1. - border_radius)) - border_radius);
    }
    return 0.;
}

fn is_border(uv: vec2<f32>) -> f32 {
     if uv.x < border_radius && uv.y < border_radius {
        return ceil(distance(uv, vec2<f32>(border_radius, border_radius)) - inner_radius);
    }
    if uv.x > 1. - border_radius && uv.y < border_radius {
        return ceil(distance(uv, vec2<f32>(1. - border_radius, border_radius)) - inner_radius);
    }
    if uv.x > 1. - border_radius && uv.y > 1. -  border_radius {
        return ceil(distance(uv, vec2<f32>(1. - border_radius, 1. - border_radius)) - inner_radius);
    }
    if uv.x < border_radius && uv.y > 1. - border_radius {
        return ceil(distance(uv, vec2<f32>(border_radius, 1. - border_radius)) - inner_radius);
    }
    let border_shade = ceil(clamp(uv.y - (1. - border), 0., 1.));
    let border_shade = max(border_shade, ceil(clamp(border - uv.y, 0., 1.)));
    let border_shade = max(border_shade, ceil(clamp(uv.x - (1. - border), 0., 1.)));
    let border_shade = max(border_shade, ceil(clamp(border - uv.x, 0., 1.)));
    return border_shade;
}

@fragment
fn fragment(
    #import bevy_pbr::mesh_vertex_output
) -> @location(0) vec4<f32> {
    let alpha = vec4<f32>(0., 0., 0., -material.color.w) * is_alpha(uv);
    let offset = ((uv.x + uv.y) / 2. - .5) * gradient_intensity;
    let offset = vec4<f32>(offset, offset, offset, 0.);
    let border = is_border(uv);
    let border = offset * (border - .5) * 2.;
    return material.color + border * .2 + alpha;
}
