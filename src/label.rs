use bevy::prelude::SystemLabel;

#[derive(SystemLabel)]
pub enum Stage {
    /// Process input.
    Input,
    /// Update state.
    Update,
    /// Draw state.
    Draw,
}
