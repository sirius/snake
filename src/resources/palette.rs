use bevy::prelude::{Color, Resource};
use lab::Lab;

use crate::{color::DistantColors, MAX_PLAYERS};

#[derive(Resource)]
pub struct Palette {
    pub background: Color,
    pub fruit: Color,
    pub players: [Color; MAX_PLAYERS],
}

impl Palette {
    pub fn new() -> Self {
        Palette {
            background: Color::MIDNIGHT_BLUE,
            fruit: Color::RED,
            players: [Color::GREEN; MAX_PLAYERS],
        }
    }

    pub fn randomize(&mut self) {
        let mut colors = DistantColors::random();
        self.background = colors
            .next()
            .expect("DistantColors should always return colors");
        self.fruit = colors
            .next()
            .expect("DistantColors should always return colors");
        for i in 0..MAX_PLAYERS {
            self.players[i] = colors
                .next()
                .expect("DistantColors should always return colors");
        }
    }
}
