use bevy::{
    prelude::Resource,
    time::{Timer, TimerMode},
};

#[derive(Resource)]
pub struct SnakeMovementTimer(pub Timer);

impl SnakeMovementTimer {
    pub fn from_seconds(duration: f32) -> Self {
        SnakeMovementTimer(Timer::from_seconds(duration, TimerMode::Repeating))
    }
}

#[derive(Resource)]
pub struct FruitTimer(pub Timer);

impl FruitTimer {
    pub fn from_seconds(duration: f32) -> Self {
        FruitTimer(Timer::from_seconds(duration, TimerMode::Repeating))
    }
}
