use bevy::prelude::{Component, KeyCode};

#[derive(Component)]
pub struct Controls {
    pub up: KeyCode,
    pub left: KeyCode,
    pub down: KeyCode,
    pub right: KeyCode,
}
