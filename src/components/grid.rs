use std::{ops::Add, slice::from_raw_parts};

use bevy::{
    prelude::{default, Color, Component, Image},
    render::{
        render_resource::{
            Extent3d, FilterMode, SamplerDescriptor, TextureDimension, TextureFormat,
        },
        texture::ImageSampler,
    },
};
use byteorder::{LittleEndian, WriteBytesExt};

#[derive(Component)]
pub struct Grid {
    pub width: u32,
    pub height: u32,
}

impl Grid {
    pub fn new(width: u32, height: u32) -> Self {
        Grid { width, height }
    }
}

#[derive(Component, Default, PartialEq, Debug)]
pub struct GridPosition {
    pub x: i32,
    pub y: i32,
}

impl Add for &GridPosition {
    type Output = GridPosition;

    fn add(self, rhs: Self) -> Self::Output {
        GridPosition {
            x: self.x + rhs.x,
            y: self.y + rhs.y,
        }
    }
}

#[derive(Component, Debug)]
pub struct GridState {
    pub width: u32,
    pub height: u32,
    color_data: Vec<Color>,
    sampler: ImageSampler,
}

impl GridState {
    pub fn new(width: u32, height: u32) -> Self {
        GridState {
            width,
            height,
            color_data: vec![Color::BLACK; (width * height) as usize],
            sampler: nearest_sampler(),
        }
    }

    pub fn clear(&mut self, color: Color) {
        self.color_data.fill(color);
    }

    pub fn set(&mut self, position: &GridPosition, color: Color) {
        let i = (position.y as u32 * self.width + position.x as u32) as usize;
        self.color_data[i] = color;
    }

    pub fn generate_texture(&self) -> Image {
        assert_eq!((self.width * self.height) as usize, self.color_data.len());
        let mut texture_data: Vec<u8> = Vec::with_capacity(self.color_data.len() * 4);
        for value in &self.color_data {
            if let Color::RgbaLinear {
                red,
                green,
                blue,
                alpha,
            } = value.as_rgba_linear()
            {
                texture_data.write_f32::<LittleEndian>(red).unwrap();
                texture_data.write_f32::<LittleEndian>(green).unwrap();
                texture_data.write_f32::<LittleEndian>(blue).unwrap();
                texture_data.write_f32::<LittleEndian>(alpha).unwrap();
            }
        }
        let mut image = Image::new(
            Extent3d {
                width: self.width,
                height: self.height,
                ..default()
            },
            TextureDimension::D2,
            texture_data,
            TextureFormat::Rgba32Float,
        );
        image.sampler_descriptor = self.sampler.clone();
        image
    }
}

fn nearest_sampler() -> ImageSampler {
    ImageSampler::Descriptor(SamplerDescriptor {
        label: Some("linear"),
        mag_filter: FilterMode::Nearest,
        min_filter: FilterMode::Nearest,
        mipmap_filter: FilterMode::Nearest,
        ..default()
    })
}
