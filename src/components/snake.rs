use bevy::prelude::Component;

use super::GridPosition;

pub enum Direction {
    North,
    East,
    South,
    West,
}

pub enum Behavior {
    Extend,
    Advance,
}

#[derive(Component)]
pub struct Snake {
    pub player: u8,
    pub segments: Vec<GridPosition>,
    direction: Direction,
}

impl Snake {
    pub fn new(player: u8, position: GridPosition, direction: Direction) -> Self {
        Snake {
            player,
            segments: vec![position],
            direction,
        }
    }

    pub fn advance(&mut self, behavior: Behavior) {
        let head = self
            .segments
            .first()
            .expect("snake must not be 0 segments long");
        let new_head = head
            + match self.direction {
                Direction::North => &GridPosition { x: 0, y: -1 },
                Direction::East => &GridPosition { x: 1, y: 0 },
                Direction::South => &GridPosition { x: 0, y: 1 },
                Direction::West => &GridPosition { x: -1, y: 0 },
            };
        if let Behavior::Advance = behavior {
            self.segments.remove(self.segments.len() - 1);
        }
        self.segments.insert(0, new_head);
    }

    pub fn turn(&mut self, direction: Direction) {
        self.direction = direction;
    }
}

#[cfg(test)]
mod tests {
    use crate::components::GridPosition;

    use super::Snake;

    #[test]
    fn advance_should_advance_north() {
        let mut snake = Snake::new(0, GridPosition { x: 0, y: 1 }, super::Direction::North);
        snake.advance(super::Behavior::Advance);
        assert_eq!(snake.segments, vec![GridPosition { x: 0, y: 0 }])
    }

    #[test]
    fn advance_should_advance_east() {
        let mut snake = Snake::new(0, GridPosition { x: 0, y: 0 }, super::Direction::East);
        snake.advance(super::Behavior::Advance);
        assert_eq!(snake.segments, vec![GridPosition { x: 1, y: 0 }])
    }

    #[test]
    fn advance_should_advance_south() {
        let mut snake = Snake::new(0, GridPosition { x: 0, y: 0 }, super::Direction::South);
        snake.advance(super::Behavior::Advance);
        assert_eq!(snake.segments, vec![GridPosition { x: 0, y: 1 }])
    }

    #[test]
    fn advance_should_advance_west() {
        let mut snake = Snake::new(0, GridPosition { x: 1, y: 0 }, super::Direction::West);
        snake.advance(super::Behavior::Advance);
        assert_eq!(snake.segments, vec![GridPosition { x: 0, y: 0 }])
    }

    #[test]
    fn advance_should_extend_north() {
        let mut snake = Snake::new(0, GridPosition { x: 0, y: 1 }, super::Direction::North);
        snake.advance(super::Behavior::Extend);
        assert_eq!(
            snake.segments,
            vec![GridPosition { x: 0, y: 0 }, GridPosition { x: 0, y: 1 }]
        )
    }

    #[test]
    fn advance_should_extend_east() {
        let mut snake = Snake::new(0, GridPosition { x: 0, y: 0 }, super::Direction::East);
        snake.advance(super::Behavior::Extend);
        assert_eq!(
            snake.segments,
            vec![GridPosition { x: 1, y: 0 }, GridPosition { x: 0, y: 0 }]
        )
    }

    #[test]
    fn advance_should_extend_south() {
        let mut snake = Snake::new(0, GridPosition { x: 0, y: 0 }, super::Direction::South);
        snake.advance(super::Behavior::Extend);
        assert_eq!(
            snake.segments,
            vec![GridPosition { x: 0, y: 1 }, GridPosition { x: 0, y: 0 }]
        )
    }

    #[test]
    fn advance_should_extend_west() {
        let mut snake = Snake::new(0, GridPosition { x: 1, y: 0 }, super::Direction::West);
        snake.advance(super::Behavior::Extend);
        assert_eq!(
            snake.segments,
            vec![GridPosition { x: 0, y: 0 }, GridPosition { x: 1, y: 0 }]
        )
    }
}
