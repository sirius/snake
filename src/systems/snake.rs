use bevy::{
    prelude::{Commands, EventReader, Input, KeyCode, Query, Res, ResMut},
    time::Time,
};

use crate::{
    components::{Behavior, Controls, Direction, GridPosition, GridState, Snake},
    resources::{Palette, SnakeMovementTimer},
    NewGameEvent, GRID_HEIGHT, GRID_WIDTH,
};

pub fn spawn_snake(mut commands: Commands, reader: EventReader<NewGameEvent>) {
    if !reader.is_empty() {
        commands
            .spawn(Snake::new(
                0,
                GridPosition {
                    x: GRID_WIDTH as i32 / 2,
                    y: GRID_HEIGHT as i32 / 2,
                },
                Direction::East,
            ))
            .insert(Controls {
                up: KeyCode::W,
                left: KeyCode::A,
                down: KeyCode::S,
                right: KeyCode::D,
            });
    }
}

pub fn snake_input(keyboard: Res<Input<KeyCode>>, mut query: Query<(&mut Snake, &Controls)>) {
    for (mut snake, controls) in &mut query {
        if keyboard.pressed(controls.up) {
            snake.turn(Direction::North);
        }
        if keyboard.pressed(controls.left) {
            snake.turn(Direction::West);
        }
        if keyboard.pressed(controls.down) {
            snake.turn(Direction::South);
        }
        if keyboard.pressed(controls.right) {
            snake.turn(Direction::East);
        }
    }
}

pub fn snake_move(
    time: Res<Time>,
    mut timer: ResMut<SnakeMovementTimer>,
    mut query: Query<&mut Snake>,
) {
    if timer.0.tick(time.delta()).just_finished() {
        for mut snake in &mut query {
            snake.advance(Behavior::Extend);
        }
    }
}

pub fn snake_draw(
    palette: Res<Palette>,
    snakes: Query<&Snake>,
    mut grid_states: Query<&mut GridState>,
) {
    for mut grid_state in &mut grid_states {
        for snake in &snakes {
            let color = palette.players[snake.player as usize];
            for segment in &snake.segments {
                if segment.x >= 0
                    && (segment.x as u32) < grid_state.width
                    && segment.y >= 0
                    && (segment.y as u32) < grid_state.height
                {
                    grid_state.set(segment, color);
                }
            }
        }
    }
}
