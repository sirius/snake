use std::mem;

use bevy::{
    prelude::{
        default, shape, Assets, Commands, EventReader, Handle, Image, Mesh, Query, Res, ResMut,
        SpatialBundle, Transform, With,
    },
    render::render_resource::{Extent3d, TextureDimension, TextureFormat},
    sprite::MaterialMesh2dBundle,
    window::Windows,
};

use crate::{
    components::{Grid, GridState},
    materials::GridMaterial,
    NewGameEvent, GRID_HEIGHT, GRID_PADDING, GRID_WIDTH,
};

const COLOR_F: [f32; 4] = [0.0, 0.0, 0.0, 1.0];
const COLOR: [u8; 16] = unsafe { mem::transmute(COLOR_F) };

pub fn spawn_grid(
    mut commands: Commands,
    mut meshes: ResMut<Assets<Mesh>>,
    mut materials: ResMut<Assets<GridMaterial>>,
    mut textures: ResMut<Assets<Image>>,
    reader: EventReader<NewGameEvent>,
) {
    if !reader.is_empty() {
        reader.clear();
        commands
            .spawn(Grid::new(GRID_WIDTH, GRID_HEIGHT))
            .insert(SpatialBundle::default())
            .insert(GridState::new(GRID_WIDTH, GRID_HEIGHT))
            .insert(MaterialMesh2dBundle {
                mesh: meshes.add(Mesh::from(shape::Quad::default())).into(),
                transform: Transform::default(),
                material: materials.add(GridMaterial {
                    data: textures.add(Image::new_fill(
                        Extent3d::default(),
                        TextureDimension::D2,
                        &COLOR,
                        TextureFormat::Rgba32Float,
                    )),
                }),
                ..default()
            });
    }
}

pub fn resize_grid(windows: Res<Windows>, mut grids: Query<(&Grid, &mut Transform), With<Grid>>) {
    if grids.is_empty() {
        return;
    }

    let window = windows.primary();
    let (grid, mut transform) = grids.single_mut();

    let window_ratio = window.width() / window.height();
    let grid_ratio = grid.width as f32 / grid.height as f32;
    let grid_width = if window_ratio > grid_ratio {
        grid_ratio * window.height()
    } else {
        window.width()
    };
    let grid_height = if window_ratio > grid_ratio {
        window.height()
    } else {
        window.width() / grid_ratio
    };

    transform.scale.x = grid_width - GRID_PADDING * 2.0;
    transform.scale.y = grid_height - GRID_PADDING * 2.0;
}

pub fn update_grid_material(
    mut materials: ResMut<Assets<GridMaterial>>,
    mut textures: ResMut<Assets<Image>>,
    query: Query<(&Handle<GridMaterial>, &GridState)>,
) {
    for (material, state) in &query {
        let material = materials
            .get_mut(material)
            .expect("material must be registered");
        textures.remove(&material.data);
        material.data = textures.add(state.generate_texture());
    }
}
