use crate::components::{GridPosition, GridState};
use crate::{resources::Palette, NewGameEvent, PaletteUpdateEvent};
use bevy::prelude::{ClearColor, Color, EventReader, EventWriter, Query, Res, ResMut};

pub fn update_palette(
    mut palette: ResMut<Palette>,
    mut writer: EventWriter<PaletteUpdateEvent>,
    reader: EventReader<NewGameEvent>,
) {
    if !reader.is_empty() {
        palette.randomize();
        writer.send(PaletteUpdateEvent)
    }
}

pub fn update_clear_color(
    palette: Res<Palette>,
    mut clear_color: ResMut<ClearColor>,
    reader: EventReader<PaletteUpdateEvent>,
) {
    if !reader.is_empty() {
        clear_color.0 = palette.background;
    }
}

pub fn update_grid_color(
    palette: Res<Palette>,
    mut query: Query<&mut GridState>,
    reader: EventReader<PaletteUpdateEvent>,
) {
    if !reader.is_empty() {
        for mut state in &mut query {
            state.clear(palette.background);
        }
    }
}
