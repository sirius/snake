use bevy::{prelude::*, sprite::Material2dPlugin, window::close_on_esc};
use label::Stage;
use materials::GridMaterial;
use resources::{FruitTimer, Palette, SnakeMovementTimer};
use systems::{
    resize_grid, snake_draw, snake_input, snake_move, spawn_grid, spawn_snake, update_clear_color,
    update_grid_color, update_grid_material, update_palette,
};

mod color;
mod components;
mod label;
mod materials;
mod resources;
mod systems;

const MAX_PLAYERS: usize = 8;
const GRID_WIDTH: u32 = 16;
const GRID_HEIGHT: u32 = 9;
const GRID_PADDING: f32 = 20.;
const SNAKE_MOVEMENT_DELAY: f32 = 0.8;
const FRUIT_SPAWN_DELAY: f32 = 10.0;

pub struct NewGameEvent;
pub struct PaletteUpdateEvent;

fn main() {
    App::new()
        .add_plugins(DefaultPlugins)
        .add_plugin(Material2dPlugin::<GridMaterial>::default())
        .add_event::<NewGameEvent>()
        .add_event::<PaletteUpdateEvent>()
        .insert_resource(ClearColor(Color::BLACK))
        .insert_resource(Palette::new())
        .insert_resource(SnakeMovementTimer::from_seconds(SNAKE_MOVEMENT_DELAY))
        .insert_resource(FruitTimer::from_seconds(FRUIT_SPAWN_DELAY))
        .add_system(close_on_esc)
        .add_startup_system(setup)
        .add_startup_system(start_game)
        .add_system(update_clear_color)
        .add_system(spawn_grid)
        .add_system(resize_grid)
        .add_system(spawn_snake)
        .add_system(snake_input.label(Stage::Input))
        .add_system(update_palette.label(Stage::Update))
        .add_system(update_grid_color.label(Stage::Update))
        .add_system(snake_move.label(Stage::Update).after(Stage::Input))
        .add_system(snake_draw.label(Stage::Draw).after(Stage::Update))
        .add_system(update_grid_material.after(Stage::Draw))
        .run();
}

fn start_game(mut writer: EventWriter<NewGameEvent>) {
    writer.send(NewGameEvent);
}

fn setup(mut commands: Commands) {
    commands.spawn(Camera2dBundle::default());
}
