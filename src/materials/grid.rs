use bevy::{
    prelude::{Handle, Image},
    reflect::TypeUuid,
    render::render_resource::{AsBindGroup, ShaderRef},
    sprite::Material2d,
};

#[derive(AsBindGroup, TypeUuid, Clone)]
#[uuid = "95e3b948-41a7-4b83-bfd5-2ff1fae76600"]
pub struct GridMaterial {
    #[texture(0)]
    #[sampler(1)]
    pub data: Handle<Image>,
}

impl Material2d for GridMaterial {
    fn fragment_shader() -> ShaderRef {
        "shaders/grid_material.wgsl".into()
    }
}
