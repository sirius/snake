use bevy::prelude::Color;
use lab::Lab;
use rand::{thread_rng, Rng};

const L_MIN: f32 = 0.0;
const L_MAX: f32 = 100.0;
const L_RANGE: f32 = L_MAX - L_MIN;
const A_MIN: f32 = -128.0;
const A_MAX: f32 = 127.0;
const A_RANGE: f32 = A_MAX - A_MIN;
const B_MIN: f32 = -128.0;
const B_MAX: f32 = 127.0;
const B_RANGE: f32 = B_MAX - B_MIN;

const COMPONENT_PRECEDENCE: [[u8; 3]; 3] = [
    [0b001, 0b100, 0b010],
    [0b100, 0b010, 0b001],
    [0b010, 0b001, 0b100],
];

pub struct DistantColors {
    seed: Lab,
    layer: usize,
    dimensions: u8,
    next_index: usize,
}

impl DistantColors {
    pub fn new(seed: Lab) -> Self {
        DistantColors {
            seed,
            layer: 0,
            dimensions: 0,
            next_index: 0,
        }
    }

    pub fn random() -> Self {
        Self::new(random_color())
    }
}

impl Iterator for DistantColors {
    type Item = Color;

    fn next(&mut self) -> Option<Self::Item> {
        if self.next_index > 1 << self.layer {
            self.layer += 1;
            self.next_index = 1;
        }

        let component_selector = COMPONENT_PRECEDENCE[self.layer % 3];

        let l = if self.dimensions & component_selector[0] > 0 {
            let l_offset = L_RANGE / (1 << self.layer) as f32;
            (self.seed.l + l_offset * self.next_index as f32 - L_MIN) % L_RANGE + L_MIN
        } else {
            self.seed.l
        };
        let a = if self.dimensions & component_selector[1] > 0 {
            let a_offset = A_RANGE / (1 << self.layer) as f32;
            (self.seed.a + a_offset * self.next_index as f32 - A_MIN) % A_RANGE + A_MIN
        } else {
            self.seed.a
        };
        let b = if self.dimensions & component_selector[2] > 0 {
            let b_offset = B_RANGE / (1 << self.layer) as f32;
            (self.seed.b + b_offset * self.next_index as f32 - B_MIN) % B_RANGE + B_MIN
        } else {
            self.seed.b
        };

        let next_lab = Lab { l, a, b };

        if self.dimensions > 1 {
            self.dimensions -= 1;
        } else {
            self.next_index += 2;
            self.dimensions = 0b111;
        }

        Some(lab2rgb(next_lab))
    }
}

fn random_color() -> Lab {
    let mut rng = thread_rng();
    Lab {
        l: rng.gen_range(L_MIN..L_MAX),
        a: rng.gen_range(A_MIN..A_MAX),
        b: rng.gen_range(B_MIN..B_MAX),
    }
}

fn lab2rgb(lab: Lab) -> Color {
    let rgb = lab.to_rgb();
    Color::rgb(
        rgb[0] as f32 / 255.,
        rgb[1] as f32 / 255.,
        rgb[2] as f32 / 255.,
    )
}
